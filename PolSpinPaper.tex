%-------------------------------------------------------------------------------
% This file provides a skeleton ATLAS document.
%-------------------------------------------------------------------------------

\pdfoutput=1
%-------------------------------------------------------------------------------
% Specify where ATLAS LaTeX style files can be found.
\newcommand*{\ATLASLATEXPATH}{latex/}
%-------------------------------------------------------------------------------
\documentclass[UKenglish,texlive=2011,txfonts,cernpreprint]{\ATLASLATEXPATH atlasdoc}
%-------------------------------------------------------------------------------

% Extra packages:
\usepackage{\ATLASLATEXPATH atlaspackage}
%-------------------------------------------------------------------------------
% Style file with biblatex options for ATLAS documents.
\usepackage{\ATLASLATEXPATH atlasbiblatex}

% Package for creating list of authors and contributors to the analysis.
\usepackage{\ATLASLATEXPATH atlascontribute}

% Useful macros
\usepackage{\ATLASLATEXPATH atlasphysics}
\usepackage{\ATLASLATEXPATH atlasunit}
\addbibresource{PolSpinPaper.bib}
\addbibresource{bibtex/bib/ATLAS.bib}

% Paths for figures - do not forget the / at the end of the directory name.
\graphicspath{{logos/}{figures/}}

%additional packages
\usepackage{fp} %used for rounding in the defs.sty
\usepackage{dcolumn}
\newcolumntype{d}[1]{D{.}{\cdot}{#1} }

\usepackage{PolSpinPaper-defs}
\usepackage{rotating}
\usepackage{multirow}

%-------------------------------------------------------------------------------
% Generic document information
%-------------------------------------------------------------------------------

% Title, abstract and document
\input{PolSpinPaper-metadata}
% Author and title for the PDF file
\hypersetup{pdftitle={ATLAS draft},pdfauthor={The ATLAS Collaboration}}

%-------------------------------------------------------------------------------
% Content
%-------------------------------------------------------------------------------
\begin{document}

\maketitle

\tableofcontents

%-------------------------------------------------------------------------------
\section{Introduction}
\label{sec:intro}
%-------------------------------------------------------------------------------

The top quark, discovered in 1995 by the CDF and D0 experiments at the
Tevatron at Fermilab~\cite{topdisc1,topdisc2}, is the heaviest fundamental
particle observed so far. 
Its mass is of the order of the electroweak scale, which suggests that it might play a special role in
electroweak symmetry breaking. Furthermore, since the top quark has a very short lifetime of
$\mathcal{O}(10^{-25}\text{
  s})$~\cite{lifetime_theo,lifetime_d0,lifetime_cdf} it decays before
hadronisation and before any consequent spin-flip can take place. This offers
a unique opportunity to study the properties of a bare
quark and, in particular, the properties of its spin.

Top quarks at the LHC are mostly produced in \ttbar pairs via the strong
interaction,
which conserves parity. The quarks\footnote{Antiparticles are generally included in the discussions unless otherwise stated.} and gluons of the initial state
are unpolarised, which means that their spins are not preferentially aligned with any given direction.
The top quarks produced in pairs are thus unpolarised except for the contribution of weak corrections and QCD absorptive
parts at the per-mill level~\cite{bernreuther_spin2013}. However, the spins of the top and antitop
quarks are correlated with a strength depending on the spin
quantisation axis and on the production process. Various new physics
phenomena can alter the polarisation and spin correlation due to
alternative production
mechanisms~\cite{bernreuther_spin2013,spin_obs_theo,godbole_chargedhiggs,godbole_sbottom}.
The spins of the top quarks do not become decorrelated
due to hadronisation, and so their spin information is transferred to their decay
products. This makes it possible to measure the top quark pair's 
spin structure using angular observables of their decay products.
The predictions for many of these observables are available at next-to-leading order (NLO) in quantum chromodynamics (QCD). 
A few of them have been measured by the experiments at the LHC and Tevatron and found to be in good agreement with the Standard Model (SM)
predictions~\cite{spin_atlas1,pol_atlas,Aad:2015bfa,spin_atlas2,spinpol_cms,Khachatryan:2015tzo,spinpol_cms8TeV,pol_d0_ljets,spin_cdf}.

This paper presents the measurement of a set of 15 spin observables with
a data set corresponding to an integrated luminosity of $20.2\text{ fb}^{-1}$ of proton--proton collisions
at $\sqrt{s} = 8$ \TeV, recorded by the ATLAS detector at the LHC in 2012. Each
of the 15 observables is sensitive to a different coefficient of the top
quark pair's spin density matrix, probing different symmetries in the
production mechanism~\cite{bernreuther_observables}. Ten of these observables have not
been measured until now. The observables are corrected back to parton level in the full phase-space and
to stable-particle level in a fiducial phase-space.
At parton level, the measured values of the polarisation
and spin correlation observables are presented and compared to
theoretical predictions. All observables allow a direct
measurement of their corresponding expectation value.
At stable-particle level, the distributions corrected for detector
acceptance and resolution are provided. Because of the limited phase-space used at that level,
the values of the polarisation and spin correlations
are not proportional to the means of these distributions.
Instead, the means of the distributions are provided and compared to the values obtained in Monte Carlo simulation.

\input{PolSpinPaper/Detector}
\input{PolSpinPaper/Observables}
\input{PolSpinPaper/MCSamples}
\input{PolSpinPaper/Selection}
\input{PolSpinPaper/Analysis}



% All figures and tables should appear before the summary and conclusion.
% The package placeins provides the macro \FloatBarrier to achieve this.
% \FloatBarrier
\clearpage

%-------------------------------------------------------------------------------
\section{Conclusion}
\label{sec:conclusion}
%-------------------------------------------------------------------------------

A measurement of 15 top quark spin observables was performed using a
data set of 20.2 $\text{fb}^{-1}$ of proton--proton collisions at
$\sqrt{s} = 8 $ \TeV, recorded by the ATLAS detector at the LHC.
The analysis is performed in the dilepton final state, characterised by the
presence of two isolated leptons.
Each of the observables is sensitive to a different coefficient of the spin
density matrix of \ttbar production.
The observable distributions are
corrected back to the parton and stable-particle level. At parton
level, the measurements along the helicity axis are
\begin{eqnarray}
\helpolplus & = & \ResultHelpolPlus \pm \TotalUncHelpolPlus~[\pm~\MassUncHelpolPlus~\textrm{(mass)} ],    \nonumber \\
\helpolminus & = & \ResultHelpolMinus \pm \TotalUncHelpolMinus~[\pm~\MassUncHelpolMinus~\textrm{(mass)} ], \nonumber \\
\helcorr & = & \phantom{+}\ResultHelcorr \pm \TotalUncHelcorr~[\pm~\MassUncHelcorr~\textrm{(mass)} ]. \nonumber
\end{eqnarray}
These values are in good agreement with the NLO SM predictions of
$\helpolplus = 0.0030 \pm 0.0010$, $\helpolminus = 0.0034 \pm 0.0010$ and $\helcorr = 0.318 \pm 0.003$.
The spin correlation along the transverse axis differs from zero
with a significance of 5.1 $\sigma$.
At stable-particle level, the unfolded distributions are compared to
their prediction from MC simulation (\powheg+\pythia). All distributions are
in agreement with the predictions.


%-------------------------------------------------------------------------------
\section*{Acknowledgements}
%-------------------------------------------------------------------------------

\input{Acknowledgements}


%-------------------------------------------------------------------------------
\clearpage

%-------------------------------------------------------------------------------
\printbibliography
%-------------------------------------------------------------------------------
\clearpage

\newpage 
\input{atlas_authlist}


\end{document}
