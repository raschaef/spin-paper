\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\select@language {UKenglish}
\contentsline {section}{\numberline {1}Introduction}{2}{section.1}
\contentsline {section}{\numberline {2}ATLAS detector}{3}{section.2}
\contentsline {section}{\numberline {3}Observables}{4}{section.3}
\contentsline {section}{\numberline {4}Data and simulation samples}{6}{section.4}
\contentsline {section}{\numberline {5}Event selection and background estimation}{7}{section.5}
\contentsline {subsection}{\numberline {5.1}Object selection}{7}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Event selection}{8}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Background estimation}{8}{subsection.5.3}
\contentsline {subsection}{\numberline {5.4}Kinematic reconstruction of the $t\bar {t}$ system}{9}{subsection.5.4}
\contentsline {subsection}{\numberline {5.5}Event yields and kinematic distributions}{10}{subsection.5.5}
\contentsline {section}{\numberline {6}Analysis}{12}{section.6}
\contentsline {subsection}{\numberline {6.1}Truth level definitions}{12}{subsection.6.1}
\contentsline {subsubsection}{\numberline {6.1.1}Parton-level definition}{12}{subsubsection.6.1.1}
\contentsline {subsubsection}{\numberline {6.1.2}Stable-particle definition and fiducial region}{12}{subsubsection.6.1.2}
\contentsline {subsection}{\numberline {6.2}Unfolding}{13}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Systematic uncertainties}{14}{subsection.6.3}
\contentsline {subsubsection}{\numberline {6.3.1}Detector modelling uncertainties}{17}{subsubsection.6.3.1}
\contentsline {subsubsection}{\numberline {6.3.2}Background-related uncertainties}{18}{subsubsection.6.3.2}
\contentsline {subsubsection}{\numberline {6.3.3}Modelling uncertainties}{18}{subsubsection.6.3.3}
\contentsline {subsubsection}{\numberline {6.3.4}Other uncertainties}{19}{subsubsection.6.3.4}
\contentsline {section}{\numberline {7}Results}{20}{section.7}
\contentsline {section}{\numberline {8}Conclusion}{27}{section.8}
