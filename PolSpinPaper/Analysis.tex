\section{Analysis}
\label{sec:analysis}

Two different measurements of the spin observables are performed.  One
set of measurements is corrected to parton level and the other set is corrected 
to stable-particle level. 
These two levels are defined in the next section, as well as the phase-spaces to which 
the measurements are corrected.

\subsection{Truth level definitions}

\subsubsection{Parton-level definition}
At parton level, the considered top quarks are 
taken from the MC history after radiation but before decay. 
Parton-level leptons include
tau leptons before they decay into an electron or muon and before radiation.
With these definitions, the polarisation can be extracted from the
slope of the \costheta distribution of parton-level particles 
(Equation~(\ref{eqn:single-diff})) and the correlation can be extracted from the mean
value of the distribution (Equation~(\ref{eqn:spin_relation})). 
The measurement corrected to parton level is extrapolated to the full phase-space, where all generated dilepton events are considered.

\subsubsection{Stable-particle definition and fiducial region}
Stable-particle level includes only particles with a lifetime larger than 30~ps.
The charged leptons are required not to originate from hadrons.
Photons within a cone of $\Delta R = 0.1$ around the lepton direction 
are considered as bremsstrahlung and so their four-momenta are added 
to the lepton four-momentum.
Selected leptons are required to have $p_T > 25$~\GeV\ and $|\eta| < 2.5$.
Jets are clustered from all stable particles, excluding the already selected
leptons, by an anti-$k_{t}$ algorithm with a radius parameter $R=0.4$. 
Neutrinos can be clustered within jets.
Intermediate $b$-hadrons have their momentum set to zero, and are allowed to be clustered into the jets 
along with the stable particles~\cite{Cacciari:2008gn}.
If after clustering a $b$-hadron is found in a jet, the jet is considered as $b$-tagged~\cite{Cacciari:2008gn}. 
Jets must have a transverse momentum of at least 25 \GeV\ and have a
pseudorapidity of $|\eta| < 2.5$. Events are rejected if a lepton and a jet are 
separated by $\Delta R < 0.4$.
A fiducial phase-space close to the detector and selection acceptance is defined 
by requiring the presence of at least two leptons and at least two jets
satisfying the kinematic selection criteria. Around 32\% of all generated events
satisfy the fiducial requirements.
No $b$-jet is required in the definition of the fiducial region to keep it common with other analyses not using $b$-tagging. 
The $b$-jets are used in the kinematic reconstruction described in the following.

The top quarks (called pseudo-top-quarks~\cite{Aad:2015eia}) are reconstructed from
the stable particles defined above. If no jets are $b$-tagged, the two highest-\pt
jets are considered for the pseudo-top-quark reconstruction. 
Neutrinos are required not to originate from hadrons, but from $W$ or $Z$
decays or from intermediate tau decays.
For the reconstruction, only
the two neutrinos with the highest \pt are taken in MC events. 
The correct lepton--neutrino pairings are chosen as those with reconstructed masses 
closer to the $W$ boson mass. The correct jet--lepton--neutrino pairings are 
then chosen as those with masses closer to the
generated top quark mass of 172.5 \GeV.

In contrast to the parton-level measurement where all events are included,
   events from outside the fiducial region can still pass the event selection at reconstruction level
   and have to be treated as additional background (called the
       non-fiducial background). This contribution is estimated from 
   background-subtracted data by applying the binwise ratio of non-fiducial to
   total reconstructed signal events obtained from MC simulation, which is found to be
   constant for different levels of polarisation and correlation with
   an average of about 6.5\%.
\input{PolSpinPaper/Unfolding}
\input{PolSpinPaper/Systematics}
\input{PolSpinPaper/Results}
