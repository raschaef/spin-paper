\subsection{Systematic uncertainties}
\label{sec:systematics}

The measurement of the spin observables is affected in various ways by
systematic uncertainties. Three different types of systematic uncertainties 
are considered: detector modelling uncertainties affecting both the signal and
background, normalisation uncertainties of the background, and modelling
uncertainties of the signal. The
first two types are included in the marginalisation procedure. 
The reconstructed distribution, varied to reflect a systematic uncertainty,
is compared to the nominal distribution and the average change per bin 
is taken as the width of the Gaussian prior, as discussed in
Section~\ref{sec:unfolding}. 
In order to estimate the impact of each source of systematic uncertainty
individually, pseudodata corresponding to the sum of the nominal signal and
background samples are used. The unfolding procedure with marginalisation
is applied to the pseudodata and constraints on the systematic uncertainties are
obtained. The strongest constraint is on the uncertainty related to the electron identification 
and it reduces this systematic uncertainty by 50\%. The other constraints are of the order of a few percent.
The constrained systematic uncertainties are then used to build the $\pm$1$\sigma$
variations of the prediction. The varied pseudodata are then unfolded
without marginalisation. The impact of each systematic uncertainty is computed by taking half of the
difference between the results obtained from the $\pm$1$\sigma$ variations of pseudodata.

Modelling systematic uncertainties for the signal process are estimated separately by building calibration curves for each sample.
The unfolded value in data is calibrated to generator level using the calibration curves for the nominal sample and the sample varied to reflect the uncertainty.
The difference is taken as the systematic uncertainty.

\subsubsection{Detector modelling uncertainties}
All sources of detector modelling uncertainty are discussed below.

  \subsubsection*{Lepton-related uncertainties}

  \begin{itemize}


  \item{ \textbf{Reconstruction, identification and trigger.}
      The reconstruction and identification efficiencies for electrons and muons, as well as the efficiency of the
      triggers used to record the events differ between data and simulation. Scale factors and their uncertainties are derived using
      tag-and-probe techniques on $Z \rightarrow \ell^{+} \ell^{-} (\ell = e~\text{or}~\mu)$ events in data and in simulated samples to correct the simulation for these differences~\cite{atlas_trigger_perf,atlas_elec_sf}.
    }
  \item{ \textbf{Momentum scale and resolution}. The accuracy of the lepton
    momentum scale and resolution in simulation is checked using
      the  $Z \rightarrow \ell^{+} \ell^{-}$ and $J/\Psi \rightarrow
      \ell^{+} \ell^{-} $ invariant mass distributions. In the case of electrons, $E/p$ studies using
      $W \rightarrow e\nu$ events are also used. Small differences are observed
      between data and simulation. Corrections to the lepton energy scale and
      resolution, and their related uncertainties are also
      considered~\cite{atlas_trigger_perf,atlas_elec_sf}.
    }
  \end{itemize}
  
  \subsubsection*{Jet-related uncertainties}
  
  \begin{itemize}
  \item{ \textbf{Reconstruction efficiency}.
      The jet reconstruction efficiency is found to be about $0.2\%$ lower in the simulation than in data for jets
      below 30~\GeV\ and it is consistent with data for higher jet \pt. To evaluate the systematic uncertainty due to
      this small inefficiency, $0.2\%$ of the jets with \pt below 30~\GeV\ are removed randomly and all jet-related
      kinematic variables (including the missing transverse momentum) are recomputed. The event selection is
repeated using the modified number of jets.
}
\item{ \textbf{Vertex fraction efficiency}.
    The per-jet efficiency to satisfy the jet vertex fraction requirement is measured in $Z \rightarrow \ell^{+} \ell^{-}+\textrm{1-jet}$ events
    in data and simulation, selecting separately events enriched in hard-scatter jets and events enriched in
    jets from pile-up. The corresponding uncertainty
    is estimated by changing the nominal JVF requirement value and
    repeating the analysis using the modified value.
  }
\item{ \textbf{Energy scale}. The jet energy scale (JES) and its uncertainty were derived by combining information from test-beam
    data, LHC collision data and simulation~\cite{atlas_jes}. The jet energy scale uncertainty is split
    into 22 uncorrelated sources, which have different jet $p_\text{T}$ and $\eta$ dependencies
    and are treated independently.
  }
  
\item{\textbf{Energy resolution}. The jet energy resolution was measured
  separately for data and simulation.  A systematic
    uncertainty is defined as the difference in quadrature between the jet energy
    resolutions for data and simulation. To estimate the corresponding
    systematic uncertainty, the jet energy in 
    simulation is smeared by this residual difference. 
}

\item{\textbf{$b$-tagging/mistag efficiency}. Efficiencies to tag jets from $b$- and $c$-quarks in the simulation are
    corrected by \pt- and $\eta$-dependent data/MC scale factors.
    The uncertainties on these scale factors are about 2\% for the
    efficiency for $b$-jets, between 8\% and 15\% for $c$-jets, and between 15\%
    and 43\% for light jets~\cite{atlas_btagging1,atlas_btagging2}. 
  }
\end{itemize}

The dominant uncertainties in this category are related to lepton reconstruction, identification and trigger, jet energy scale and jet energy resolution.
The contribution from this category to the total uncertainty is small (less than 20\% for all observables).

\subsubsection*{Missing transverse momentum}

The systematic uncertainties associated with the momenta and energies
of reconstructed objects (leptons and jets) are propagated to the $\met$ calculation. The $\met$ reconstruction
also receives contributions from the presence of low-\pt jets and
calorimeter energy deposits not included in reconstructed objects (the ``soft term''). 
The systematic uncertainty associated with the soft term is estimated using
$Z \rightarrow \mu^{+} \mu^{-}$ events using methods similar to those used in Ref.~\cite{atlas_met2}.
The effect of this procedure on the measured observables is minor.

\subsubsection{Background-related uncertainties}

The uncertainties on the single-top-quark, $t\bar{t}V$, and diboson backgrounds are 6.8\%, 10\%, and 5\%, respectively~\cite{Kidonakis:2012db,Campbell:2012dh,Garzelli:2012bn}. 
These correspond to the uncertainties on the theoretical cross
sections used to normalise the MC simulated samples.

The uncertainty on the normalisation of the fake-lepton background is
estimated by using various MC generators for each process
contributing to this background. The scale factor in the control region is recomputed for each variation and the change is propagated to the expected number of events in the signal region.
In the $\mu\mu$ channel, the uncertainty is obtained by comparing a
purely data-driven method based on the measurement of the
efficiencies of real and fake loose leptons,
 and the estimation used in this analysis.   
The resulting relative total uncertainties are 170\% in the $ee$ channel,
77\% in the $\mu\mu$ channel and 49\% in the $e\mu$ channel. 

For the Drell--Yan background the detector modelling uncertainties described previously are propagated to the scale factors 
derived in the control region by recalculating them for all the uncertainties.
An additional uncertainty of 5\% is obtained by varying the control region.

Uncertainties on the shape of the different backgrounds were also estimated but found to be negligible.
This category represents a minor source of uncertainty on the measurements.

\subsubsection{Modelling uncertainties}
\label{sec:modellingsysts}

These systematic uncertainties are estimated using the samples listed in Table~\ref{tab:mc_gens_signal}.

\begin{itemize}
\item {\textbf{Choice of MC generator.} The uncertainty is obtained by comparing samples generated with either the \powheg or the \mcatnlo generator, both interfaced with \herwig. It is one of the dominant uncertainties of the measurement.}
  
\item {\textbf{Parton shower and hadronisation.} This effect is estimated by comparing samples generated with \powheg interfaced either with \pythia or \herwig, and is one of the dominant systematic uncertainties.}
  
\item {\textbf{Initial- and final-state radiations.} The uncertainty associated with the ISR/FSR
    modelling is estimated using \powheg interfaced with \pythia  
    where the parameters of the generation were varied to be compatible with the results of a measurement of $t\bar{t}$ production 
    with a veto on additional jet activity in a central rapidity interval~\cite{atlas_isrfsr}.
    The difference obtained between the two samples is divided by two. 
    This uncertainty is large and even dominant for some of the observables.
}

\item {\textbf{Colour reconnection and underlying event.}}  The
  uncertainties associated with colour reconnection and the underlying event
  are obtained by comparing dedicated samples with a varied 
  colour-reconnection strength and underlying-event activity to a reference
  sample. All samples are generated by \powheg and interfaced with
  \pythia. The reference sample uses the Perugia2012 tune, 
  the colour-reconnection sample uses the Perugia2012loCR tune, and the 
  underlying-event sample uses the Perugia2012mpiHi tune.  This uncertainty is large and even dominant for some of the observables.
  
 \item {\textbf{Parton distribution functions.} PDF uncertainties are obtained by using the error sets of CT10~\cite{ct10}, MWST2008~\cite{mstw}, and NNPDF23~\cite{nnpdf}, 
        and following the recommendations of the PDF4LHC working group~\cite{PDF4LHC}.
        The impact of this uncertainty is small.}
     
\item {\textbf{Top quark \pt modelling.} The top quark \pt spectrum is not satisfactorily modelled in MC simulation~\cite{Aad:2015mbv,Khachatryan:2015oqa}. 
       The impact of the mismodelling is estimated by reweighting the simulation to data and unfolding the different distributions using the nominal response matrix.
       The differences with respect to the nominal values are negligible compared to the other modelling uncertainties. 
       The impact of this mismodelling is thus considered negligible, and no uncertainty is added to the total uncertainty.}

      
    \item{\textbf{Polarisation and spin correlation.}} The response
      matrices used in the unfolding are calculated using the SM
      polarisation and spin correlation. An uncertainty related to a
      different polarisation and spin correlation is obtained by
      changing their values in the linearity test. In the reweighting
      procedure of the spin correlation observables, the polarisation
      is changed by $\pm 0.5$\%, while for the polarisation
      observables, the spin correlation is changed by $\pm 0.1$. This
      uncertainty cannot be applied to the cross correlation
      observables as no analytic description of these observables is
      available. Instead, a linear reweighting is used, not depending on the
      polarisation or spin correlation along any axis as described in Section~\ref{sec:unfolding}.
    \end{itemize}

The impact of this category is large and can represent up to 85\% of the total uncertainty.
    
    \subsubsection{Other uncertainties}
  \begin{itemize}
  \item{\textbf{Non-closure uncertainties.}} 
  When the calibration curve for the nominal signal \powheg sample is estimated
  a residual slope and a non-zero offset are observed. This bias, introduced by
  the unfolding procedure, is propagated to the measured values. This uncertainty is small compared to the total uncertainty.
  \item{\textbf{MC sample size.}}
  The statistical uncertainty of the nominal signal \powheg sample is estimated by
  performing pseudoexperiments on MC events. The migration matrix is varied within the MC statistical
  uncertainty and the unfolding procedure is repeated. The standard deviation of the unfolded polarisation or spin correlation values 
  is taken as the uncertainty. This uncertainty is small compared to the total uncertainty.
  \item{\textbf{Top quark mass uncertainty.}}
  The top quark mass is assumed to be 172.5 \GeV\ in MC simulation and in the reconstruction method. 
  A variation of this value could have an impact on the measurement.
  To estimate this impact, MC samples with different values of the top quark mass are unfolded with the default response matrix. 
  For each observable, the dependence of the unfolded value on the mass is fitted with a linear function and presented in Section~\ref{sec:results}.
  The slope is then multiplied by the 0.70 \GeV\ uncertainty on the most precise ATLAS top quark mass measurements~\cite{Aaboud:2016igd}.
  The obtained uncertainty is presented in the next section, but it is shown separately and is not included in the total uncertainty.
  \end{itemize}
  
