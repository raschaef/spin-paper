\section{Results}
\label{sec:results}

Applying the unfolding procedure with marginalisation to the
reconstructed distributions gives the following results at parton and
stable-particle level. Table~\ref{tab:results_summary} presents the results for the
polarisations and correlations at parton level. It shows the central value and the total uncertainty
as well as a breakdown of the systematic uncertainties for the various categories described in 
Section~\ref{sec:systematics}.
Figure~\ref{fig:results_summary_partonic} shows 
the predictions at 8 \TeV\ calculated
in Ref.~\cite{bernreuther_observables} and the unfolded result.
None of the observables deviate significantly from the SM predictions. 
The transverse correlation, \transcorr, 
differs from the case of no spin correlation by
$5.1$ standard deviations. The correlations between the different polarisation and spin correlations were
evaluated and found to be small. The highest correlations are found to be around 10\% 
between the polarisation and spin correlation along the
helicity axis and the $r$-axis and between some cross correlations. 

Figures~\ref{fig:results_particle_pol} to~\ref{fig:results_particle_crosscorr} show the observable distributions corrected
back to stable-particle level and compared to the generated
distribution created from \powheg\!+\pythia. 
No significant difference between the shapes of the observed and predicted distributions is observed.
The means of the distributions are compared between
unfolded data and MC predictions.
They are presented in Table~\ref{tab:results_summary}. 
In order
to compare the size of the uncertainties with the parton level measurement, the means
of the polarisation observables are multiplied by a factor of 3
and the correlations by a factor of $-9$ (Section~\ref{sec:observables}). 
Overall the total uncertainties for the measurements at parton and particle level are comparable.
The mass uncertainty is shown separately and not added to the total uncertainty, as explained in Section~\ref{sec:systematics}.
The dependence of the measured polarisations and spin correlations on the MC top quark
mass is presented in Table~\ref{tab:massSlope}. The measurements presented in this paper are compatible with other direct measurements in terms of
central values and uncertainties for the
polarisations along the helicity and transverse axis as well as for the
spin correlation along the helicity axis (Table~\ref{tab:measurements}).

\begin{center}
\begin{table}
\renewcommand\arraystretch{1.4}
\centering
\scriptsize
\begin{tabular}{|c|c|ccccc|c|}
\hline
Measurements & \text{Central}  & Total & Statistical & Detector & Modelling & Others & Mass\\ \hline 
\multicolumn{8}{c}{\rule{0pt}{4ex} Full phase-space}\\[3ex] \hline


$ \helpolplus $    & $-0.044$           &   $\pm 0.038$ &  $\pm 0.018$ &   $\pm 0.001$ &  $\pm 0.026$ &  $\pm 0.007$ &  $ \pm 0.027 $\\ 
$ \helpolminus $   & $-0.064$           &   $\pm 0.040$ &  $\pm 0.020$ &   $\pm 0.001$ &  $\pm 0.023$ &  $\pm 0.014$ &  $ \pm 0.027 $\\ 
$ \transpolplus $  & $-0.018$           &   $\pm 0.034$ &  $\pm 0.020$ &   $\pm 0.001$ &  $\pm 0.024$ &  $\pm 0.005$ &  -            \\ 
$ \transpolminus $ & $\phantom{+}0.023$ &   $\pm 0.042$ &  $\pm 0.020$ &   $\pm 0.001$ &  $\pm 0.034$ &  $\pm 0.005$ &  -            \\ 
$ \rpolplus $      & $\phantom{+}0.039$ &   $\pm 0.042$ &  $\pm 0.026$ &   $\pm 0.001$ &  $\pm 0.029$ &  $\pm 0.005$ &  -            \\ 
$ \rpolminus $     & $\phantom{+}0.033$ &   $\pm 0.054$ &  $\pm 0.023$ &   $\pm 0.002$ &  $\pm 0.045$ &  $\pm 0.006$ &  $ \pm 0.016 $\\ 
$ \helcorr $       & $\phantom{+}0.296$ &   $\pm 0.093$ &  $\pm 0.052$ &   $\pm 0.006$ &  $\pm 0.057$ &  $\pm 0.011$ &  $ \pm 0.037 $\\ 
$ \transcorr $     & $\phantom{+}0.304$ &   $\pm 0.060$ &  $\pm 0.028$ &   $\pm 0.001$ &  $\pm 0.047$ &  $\pm 0.001$ &  $ \pm 0.010 $\\ 
$ \rcorr $         & $\phantom{+}0.086$ &   $\pm 0.144$ &  $\pm 0.055$ &   $\pm 0.005$ &  $\pm 0.122$ &  $\pm 0.016$ &  $ \pm 0.038 $\\ 
$ \transhelsum $   & $-0.012$           &   $\pm 0.128$ &  $\pm 0.072$ &   $\pm0.005$ &  $\pm 0.087$ &  $\pm 0.029$ &  -             \\ 
$ \transheldiff $  & $-0.040$           &   $\pm 0.087$ &  $\pm 0.053$ &   $\pm0.004$ &  $\pm 0.058$ &  $\pm 0.003$ &  -             \\ 
$ \transrsum $     & $\phantom{+}0.117$ &   $\pm 0.132$ &  $\pm 0.070$ &   $\pm 0.003$ &  $\pm 0.102$ &  $\pm 0.010$ &  $ \pm 0.010 $\\ 
$ \transrdiff $    & $-0.006$           &   $\pm 0.108$ &  $\pm 0.069$ &   $\pm 0.005$ &  $\pm 0.070$ &  $\pm 0.004$ &  $ \pm 0.043 $\\ 
$ \rhelsum $       & $-0.261$           &   $\pm 0.176$ &  $\pm 0.083$ & $\pm0.006$ &  $\pm 0.135$ &  $\pm 0.011$ &  $ \pm 0.065 $   \\ 
$ \rheldiff $      & $\phantom{+}0.073$ &   $\pm 0.192$ &  $\pm 0.087$ &   $\pm 0.007$ &  $\pm 0.148$ &  $\pm 0.005$ &  $ \pm 0.025 $ \\ \hline

\multicolumn{8}{c}{\rule{0pt}{4ex} Fiducial phase-space}\\[3ex] \hline


 3$\langle \cos\theta_{+}^{k} \rangle$                                                           & $\phantom{+}0.125$ & $\pm 0.044$ &  $\pm 0.018$ &  $\pm 0.007$ &  $\pm 0.025$ &  $\pm 0.020$ &  $ \pm 0.027 $\\ %\hline
 3$\langle \cos\theta_{-}^{k} \rangle$                                                           & $\phantom{+}0.119$ & $\pm 0.040$ &  $\pm 0.022$ &  $\pm 0.008$ &  $\pm 0.021$ &  $\pm 0.014$ &  $ \pm 0.027 $\\ %\hline
 3$\langle \cos\theta_{+}^{n} \rangle$                                                           & $-0.025$           & $\pm 0.042$ &  $\pm 0.024$ &  $\pm 0.001$ &  $\pm 0.027$ &  $\pm 0.005$ &  -            \\ %\hline
 3$\langle \cos\theta_{-}^{n} \rangle$                                                           & $\phantom{+}0.023$ & $\pm 0.046$ &  $\pm 0.024$ &  $\pm 0.001$ &  $\pm 0.036$ &  $\pm 0.006$ &  -            \\ %\hline
 3$\langle \cos\theta_{+}^{r} \rangle$                                                           & $-0.104$           & $\pm 0.045$ &  $\pm 0.027$ &  $\pm 0.008$ &  $\pm 0.030$ &  $\pm 0.006$ &  -            \\ %\hline
 3$\langle \cos\theta_{-}^{r} \rangle$                                                           & $-0.110$           & $\pm 0.060$ &  $\pm 0.024$ &  $\pm 0.008$ &  $\pm 0.050$ &  $\pm 0.010$ &  $ \pm 0.015 $\\ %\hline
-9$\langle \cos\theta_{+}^{k}\cos\theta_{-}^{k} \rangle$                                         & $\phantom{+}0.172$ & $\pm 0.078$ &  $\pm 0.041$ &  $\pm 0.016$ &  $\pm 0.050$ &  $\pm 0.017$ &  $ \pm 0.027 $\\ %\hline
-9$\langle \cos\theta_{+}^{n}\cos\theta_{-}^{n} \rangle$                                         & $\phantom{+}0.427$ & $\pm 0.079$ &  $\pm 0.034$ &  $\pm 0.011$ &  $\pm 0.065$ &  $\pm 0.004$ &  $ \pm 0.027 $\\ %\hline
-9$\langle \cos\theta_{+}^{r}\cos\theta_{-}^{r} \rangle$                                         & $\phantom{+}0.031$ & $\pm 0.144$ &  $\pm 0.055$ &  $\pm 0.005$ &  $\pm 0.124$ &  $\pm 0.020$ &  $ \pm 0.033 $\\ %\hline
-9$\langle \cos\theta_{+}^{n}\cos\theta_{-}^{k} + \cos\theta_{+}^{k}\cos\theta_{-}^{n} \rangle$  & $\phantom{+}0.024$ & $\pm 0.132$ &  $\pm 0.078$ &  $\pm 0.004$ &  $\pm 0.085$ &  $\pm 0.025$ & -             \\ %\hline
-9$\langle \cos\theta_{+}^{n}\cos\theta_{-}^{k}-\cos\theta_{+}^{k}\cos\theta_{-}^{n} \rangle$    & $-0.047$           & $\pm 0.096$ &  $\pm 0.059$ &  $\pm 0.004$ &  $\pm 0.065$ &  $\pm 0.002$ & -             \\ %\hline
-9$\langle \cos\theta_{+}^{n}\cos\theta_{-}^{r} + \cos \theta_{+}^{r}\cos\theta_{-}^{n} \rangle$ & $\phantom{+}0.113$ & $\pm 0.143$ &  $\pm 0.076$ &  $\pm 0.005$ &  $\pm 0.108$ &  $\pm 0.023$ &  $ \pm 0.015 $\\ %\hline
-9$\langle \cos\theta_{+}^{n}\cos\theta_{-}^{r} - \cos \theta_{+}^{r}\cos\theta_{-}^{n} \rangle$ & $-0.030$           & $\pm 0.118$ &  $\pm 0.076$ &  $\pm 0.005$ &  $\pm 0.077$ &  $\pm 0.007$ &  $ \pm 0.052 $\\ %\hline
-9$\langle \cos\theta_{+}^{r}\cos\theta_{-}^{k} + \cos\theta_{+}^{k}\cos\theta_{-}^{r} \rangle$  & $-0.187$           & $\pm 0.151$ &  $\pm 0.069$ &  $\pm 0.023$ &  $\pm 0.122$ &  $\pm 0.006$ & $ \pm 0.039 $ \\ %\hline
-9$\langle \cos\theta_{+}^{r}\cos\theta_{-}^{k} - \cos\theta_{+}^{k}\cos\theta_{-}^{r} \rangle$  & $\phantom{+}0.047$ & $\pm 0.128$ &  $\pm 0.070$ &  $\pm 0.003$ &  $\pm 0.082$ &  $\pm 0.010$ & $ \pm 0.023$ \\ \hline


\end{tabular}
\caption{Results corrected to parton level in the full phase-space and to stable-particle level in the fiducial phase-space. 
  The central value with the total uncertainty is shown as well as the contribution from the various systematic uncertainty categories.
  The uncertainty from the "Background" category is not shown because it is always smaller than 0.001.
  The total uncertainty corresponds to the sum in quadrature of the uncertainty
  obtained from the unfolding procedure with marginalisation (including the
  background and detector modelling), the signal modelling and
  the "Others" category.
  The numbers shown for the "Detector" category correspond
  to the sum in quadrature of the individual estimates obtained as described in
  Section~\ref{sec:systematics}.
  The sum in quadrature of the values in the various columns thus does not necessarily match with the total uncertainty. 
  The uncertainty related to the top quark mass is presented separately. It is shown as "-" when found to be compatible with zero.
}
\label{tab:results_summary}
\end{table}
\end{center}
  

\begin{center}
	\begin{table}
		\centering
      \renewcommand{\arraystretch}{1.1}
		\begin{tabular}{ccc}
			\hline
			Measurements & Fiducial phase-space  & Full phase-space \\ \hline
			\helpolplus       & $ -0.04 \pm 0.01 $          & $ -0.04 \pm 0.01 $ \\ 
			\helpolminus      & $ -0.04 \pm 0.01 $          & $ -0.04 \pm 0.01 $ \\ 
			\transpolplus     & -                           & -                  \\ 
			\transpolminus    & -                           & -                  \\ 
			\rpolplus         & -                           & -                  \\ 
			\rpolminus        & $\phantom{+}0.02 \pm 0.01 $ & $ \phantom{+} 0.03 \pm 0.01 $ \\ 
			\helcorr          & $\phantom{+}0.04 \pm 0.01 $ & $ \phantom{+} 0.06 \pm 0.02 $ \\ 
			\transcorr        & $ -0.04 \pm 0.03 $          & $ -0.02 \pm 0.03 $ \\ 
			\rcorr            & $\phantom{+}0.05 \pm 0.03 $ & $ \phantom{+} 0.06 \pm 0.03 $ \\ 
			\transhelsum      & - & - \\ 
			\transheldiff     & - & - \\ 
			\transrsum        & $\phantom{+}0.02 \pm 0.01 $ & $ \phantom{+} 0.02 \pm 0.02 $ \\
			\transrdiff       & $ -0.08 \pm 0.01 $          & $ -0.07 \pm 0.01 $ \\ 
			\rhelsum          & $ -0.06 \pm 0.02 $          & $ -0.10 \pm 0.02 $ \\ 
			\rheldiff         & $\phantom{+}0.04 \pm 0.03 $ & $ \phantom{+} 0.04 \pm 0.03 $ \\ \hline
		\end{tabular}
    \caption{Dependence of polarisation and spin correlation measurements on the MC top quark mass. 
      The slope, computed from the reference value of 172.5 \GeV, is
      indicated for each measurement with its statistical uncertainty
      in units of $\GeV^{-1}$. %$\frac{1}{\text{\GeV}}$.
      Slopes which are compatible with zero within the uncertainty are indicated with "-".}
    \label{tab:massSlope}
	\end{table}
\end{center}

\input{tables/measurements.tex}


\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.425]{figures/results/partonic_pol_nuweight.pdf}
   \includegraphics[scale=0.425]{figures/results/partonic_corr_nuweight.pdf}
   \includegraphics[scale=0.425]{figures/results/partonic_crosscorr_nuweight.pdf}
   \caption{Comparison of the measured polarisations and spin correlations (data points) with predictions
     from the SM (diamonds) for the parton-level measurement. Inner bars indicate uncertainties obtained from the
     marginalisation, outer bars indicate modelling systematics,
     summed in quadrature. 
     The widths of the diamonds are chosen for illustrative purposes only.} 
   \label{fig:results_summary_partonic}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.325]{figures/results/big_labels/helpol_plus_data_4Bin4_rpZero_fiducial_norm_dsigma.pdf}
   \includegraphics[scale=0.325]{figures/results/big_labels/helpol_minus_data_4Bin4_rpZero_fiducial_norm_dsigma.pdf} \\
   \includegraphics[scale=0.325]{figures/results/big_labels/transpol_plus_data_4Bin1_rpZero_fiducial_norm_dsigma.pdf}
   \includegraphics[scale=0.325]{figures/results/big_labels/transpol_minus_data_4Bin1_rpZero_fiducial_norm_dsigma.pdf} \\
   \includegraphics[scale=0.325]{figures/results/big_labels/rpol_plus_data_4Bin1_rpZero_fiducial_norm_dsigma.pdf}
   \includegraphics[scale=0.325]{figures/results/big_labels/rpol_minus_data_4Bin1_rpZero_fiducial_norm_dsigma.pdf} 
   \caption{Comparison of the unfolded polarisation distributions and
     the prediction from the signal MC simulation for the stable-particle
     measurement. The total uncertainty is shown in each bin. The bin-to-bin correlations between adjacent bins are typically between $-0.9$ and $-0.6$. 
     The correlations between non-adjacent bins range from $-0.4$ to 0.6.} 
   \label{fig:results_particle_pol}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.35]{figures/results/big_labels/helcorr_data_6Bin2_rpZero_fiducial_norm_dsigma.pdf}
   \includegraphics[scale=0.35]{figures/results/big_labels/transcorr_data_6Bin2_rpZero_fiducial_norm_dsigma.pdf} \\
   \includegraphics[scale=0.35]{figures/results/big_labels/rcorr_data_6Bin2_rpZero_fiducial_norm_dsigma.pdf}
   \caption{Comparison of the unfolded spin correlation distributions and
     the prediction from the signal MC simulation for the stable-particle
     measurement. The total uncertainty is shown in each bin. 
     The bin-to-bin correlations between adjacent bins are typically between $-0.9$ and $-0.4$. 
     The correlations between non-adjacent bins range from $-0.4$ to 0.6.} 
   \label{fig:results_particle_corr}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.325]{figures/results/big_labels/transhelsum_data_6Bin1_rpZero_fiducial_norm_dsigma.pdf}
   \includegraphics[scale=0.325]{figures/results/big_labels/transheldiff_data_6Bin1_rpZero_fiducial_norm_dsigma.pdf} \\
   \includegraphics[scale=0.325]{figures/results/big_labels/transrsum_data_6Bin1_rpZero_fiducial_norm_dsigma.pdf}
   \includegraphics[scale=0.325]{figures/results/big_labels/transrdiff_data_6Bin1_rpZero_fiducial_norm_dsigma.pdf} \\
   \includegraphics[scale=0.325]{figures/results/big_labels/rhelsum_data_6Bin1_rpZero_fiducial_norm_dsigma.pdf}
   \includegraphics[scale=0.325]{figures/results/big_labels/rheldiff_data_6Bin1_rpZero_fiducial_norm_dsigma.pdf} 
   \caption{Comparison of the unfolded cross correlation distributions and
     the prediction from the signal MC simulation for the stable-particle
     measurement. The total uncertainty is shown in each bin. 
     The bin-to-bin correlations between adjacent bins are typically between $-0.9$ and $-0.7$. 
     The correlations between non-adjacent bins range from $-0.4$ to 0.6.} 
   \label{fig:results_particle_crosscorr}
 \end{center}
\end{figure}
