\section{Observables}
\label{sec:observables}

The spin information of the top quarks, encoded in the spin density matrix, is transferred to their decay particles and affects their
angular distributions. 
The spin density matrix can be expressed by a set of several coefficients:
one spin-independent coefficient, which determines the cross section and which is not measured here, three
polarisation coefficients for the top quark, three polarisation coefficients for the antitop quark, 
and nine spin correlation coefficients. 
By measuring a set of 15 polarisation and spin correlation observables, 
the coefficient functions of the squared matrix element can be probed. 
The approach used in this paper was proposed in Ref.~\cite{bernreuther_observables}.
The normalised double-differential cross section for \ttbar production
and decay is of the form~\cite{bernreuther_spin2013,Bernreuther:2004jv}
\begin{equation}
\frac{1}{\sigma} \frac{\textrm{d}^{2} \sigma}{\textrm{d} \cos \theta^{a}_{+} \textrm{d} \cos
  \theta^{b}_{-}} = \frac{1}{4} (1 +B^{a}_{+} \cos \theta^{a}_{+} +
B^{b}_{-} \cos \theta^{b}_{-} -  C(a,b) \cos
\theta^{a}_{+} \cos \theta^{b}_{-}),
\label{eqn:double-diff}
\end{equation}
where $B^{a}$, $B^{b}$ and $C(a,b)$ are the polarisations and spin correlation along the
spin quantisation axes $a$ and $b$. The angles $\theta^{a}$ and $\theta^{b}$ are defined as the angles between the momentum
direction of a top quark decay particle in its parent top quark's rest frame and the axis $a$ or $b$. The
subscript $+ (-)$ refers to the top (antitop) quark. 
From Equation~(\ref{eqn:double-diff}) one can retrieve the
following relation for the spin correlation between the axes $a$ and $b$
\begin{equation}
 C(a,b) = -9<\cos \theta_{+}^{a} \cos\theta_{-}^{b}>.
 \label{eqn:spin_relation}
\end{equation} 
Integrating out one of the
angles in Equation~(\ref{eqn:double-diff}) gives the single-differential
cross section 
\begin{equation}
  \frac{1}{\sigma} \frac{\textrm{d} \sigma}{\textrm{d} \costheta^a} = \frac{1}{2} (1 +
  B^a \costheta^a).
  \label{eqn:single-diff}
\end{equation}
This means the differential cross section has a linear dependence on the polarisation
$B^a$, from which also follows 
\begin{equation}
B^a = 3<\cos \theta^a>.
\label{eqn:pol_relation}
\end{equation}

All the observables are
based on \costheta, which is defined using three
orthogonal spin quantisation axes:
\begin{itemize}
  \item The helicity axis is defined as the top quark direction in the \ttbar rest
    frame. In Ref.~\cite{bernreuther_observables} it is
    indicated by the letter $k$, a notation which is adopted in this paper. 
    Measurements of the polarisation and spin correlation defined along
    this axis at 7 and 8 \TeV\ were consistent with the SM predictions~\cite{pol_atlas,spin_atlas1,Aad:2015bfa,spin_atlas2,spinpol_cms,Khachatryan:2015tzo, spinpol_cms8TeV}.
  \item The transverse axis is defined to be transverse to the production plane~\cite{bernreuther_spin2013,baumgart_transpol}
    created by the top quark direction and the beam axis. It is denoted by the letter $n$. 
    The polarisation along that axis was measured by the D0 experiment~\cite{pol_d0_ljets}.
  \item The $r$-axis is an axis orthogonal to the other two axes, denoted
    by the letter $r$. No observable related to this axis has been
    measured previously.
\end{itemize}

As the dominant initial state of \ttbar production at the LHC (gluon--gluon fusion) is
Bose-symmetric, \costheta calculations with respect to the transverse or $r$-axis
are multiplied by the sign of the scattering angle $y =
\hat{\mathbf{p}}\cdot \hat{\mathbf{k}}$, where $\hat{\mathbf{k}}$ is
the top quark direction in the \ttbar rest frame and $\hat{\mathbf{p}} =
(0,0,1)$, as recommended in Ref.~\cite{bernreuther_observables}. 
In the calculations of \costheta with respect to the negatively charged
lepton, the axes are multiplied by $-1$.
The observables and corresponding expectation values, as well as their SM predictions at NLO, 
are shown in Table~\ref{tab:observables}. 
The first six observables correspond to the polarisations of 
the top and antitop quarks along the various axes, the other nine
to the spin correlations. In order to distinguish between the
correlation observables, the correlations using only one
axis are referred to as spin correlations and the last six as
cross correlations.
The predictions are computed for a top quark mass of 173.34~\GeV~\cite{ATLAS:2014wva}.
In order to
measure all observables, the final-state particles of both decay
chains must be reconstructed and correctly identified. 
As charged leptons retain more information about the spin state of the top quarks, 
and as they can be precisely reconstructed, the measurement in this paper is performed in the
dileptonic final state of \ttbar events.
The charged leptons considered in this analysis are electrons or muons, either originating directly from $W$ and $Z$ decays, 
or through an intermediate $\tau$ decay.

\begin{table}
  \begin{center}
  \renewcommand{\arraystretch}{1.1}
    \begin{tabular}{ccc}
      \hline 
      Expectation values & NLO predictions & Observables \\
      \hline
      \helpolplus    &$0.0030 \pm 0.0010$ & \helcosplus         \\[0.05cm]
      \helpolminus   &$0.0034 \pm 0.0010$ & \helcosminus        \\[0.05cm]
      \transpolplus  &$0.0035\pm0.0004$   & \transcosplus       \\[0.05cm]
      \transpolminus &$0.0035\pm0.0004$   & \transcosminus      \\[0.05cm]
      \rpolplus      &$0.0013 \pm 0.0010$ & \rcosplus           \\[0.05cm]
      \rpolminus     &$0.0015 \pm0.0010$  & \rcosminus          \\[0.05cm]
      \helcorr       &$0.318 \pm 0.003$   & \helcoscos          \\[0.05cm]
      \transcorr     &$0.332 \pm 0.002$   & \transcoscos        \\[0.05cm]
      \rcorr         &$0.055 \pm 0.009$   & \rcoscos            \\[0.05cm]
      \transhelsum   &$0.0023$            & \transhelcoscossum  \\[0.05cm]
      \transheldiff  &$0$                 & \transhelcoscosdiff \\[0.05cm]
      \transrsum     &$0.0010 $           & \transrcoscossum    \\[0.05cm]
      \transrdiff    &$0$                 & \transrcoscosdiff   \\[0.05cm]
      \rhelsum       &$-0.226 \pm 0.004$  & \rhelcoscossum      \\[0.05cm]
      \rheldiff      &$0$                 & \rhelcoscosdiff     \\[0.05cm]
      \hline
      \end{tabular}
      \caption{List of the observables and corresponding expectation values measured in this analysis. 
        The SM predictions at NLO are also
        shown~\cite{bernreuther_observables}; expectation values
        predicted to be 0 at NLO are exactly 0 due to term cancellations.
        The expectation values can be obtained from the corresponding observables using the relations
        from Equations~(\ref{eqn:spin_relation}) and (\ref{eqn:pol_relation}).
        The uncertainties on the predictions refer to scale uncertainties only; values below 10$^{-4}$ are not quoted.}
   \label{tab:observables}
    \end{center}
\end{table}


