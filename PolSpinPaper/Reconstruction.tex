\subsection{Kinematic reconstruction of the $t\bar{t}$ system}
\label{sec:reco}
The dileptonic \ttbar final state consists of two charged leptons, two
neutrinos and at least two jets originating from the top quark 
decay. As the neutrinos cannot be directly observed in the detector, the kinematics
of the \ttbar
system, which is necessary to construct the observables, cannot be
simply reconstructed from the measured information. To solve the
kinematic equations and reconstruct the \ttbar system, the neutrino weighting technique~\cite{Abbott:1997fv,Aad:2015jfa} is used.

As input, the method uses the measured lepton and jet momenta. 
The masses of the top quarks are set to their generated mass of 172.5
\GeV\ whereas the masses of the $W$ bosons 
are set to their PDG values~\cite{Olive:2016xmw} in the
calculations.
A hypothesis is made for the value of the pseudorapidity of each neutrino and 
the kinematics of the system is then solved. 
For each solution found, a weight is assigned to quantify the level of agreement between 
the vectorial sum of neutrino transverse momenta and the measured \met components.
The pseudorapidities of the neutrinos are scanned independently between \mbox{$-5$} and 5 
with fixed steps of 0.025 in the range [$-2$, 2] and of 0.05 outside of that range.
All possible combinations of jets and leptons are tested.
Additionally, the resolution of the jet energy measurement is taken into account by smearing the energy of each jet 50 times. The smearing is done using transfer functions mapping the energy at particle level
to the energy after detector simulation.
Out of all the solutions obtained, the one with the highest weight is selected.
The reconstruction efficiency of the kinematic
reconstruction in the \ttbar signal sample is about 88\%.
No solution is found for the remaining events.

\subsection{Event yields and kinematic distributions}

Figure~\ref{fig:sel_dataMC} shows the jet
multiplicity, lepton \pt and jet \pt for all three channels. 
Figure~\ref{fig:reco_dataMC} shows kinematic distributions of the top quark
and the \ttbar system after the event reconstruction.
The data are well modelled by the MC predictions.
The corrections to the Drell--Yan and fake-lepton backgrounds are applied.
Only the events passing the kinematic reconstruction are 
considered in the distributions.
The total number of predicted events is slightly lower than the number of observed events, but the two are compatible within the systematic uncertainties.
The measurement is insensitive to a difference of normalisation of the signal.
There is also a slight slope in the ratio between data and prediction for the lepton and top quark \pt distributions. 
This is related to a known issue in the modelling of the top quark \pt, described in Section~\ref{sec:modellingsysts}.


\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.33]{figures/reco/all_jet_n_all_nominal.pdf}\\
   \includegraphics[scale=0.33]{figures/reco/all_jet_pt_all_nominal.pdf}
   \includegraphics[scale=0.33]{figures/reco/all_lep_pt_all_nominal.pdf}\\
   \caption{Comparison of the number of jets,
     jet \pt and lepton \pt distributions between data and
     predictions after the event
     selection in the combined dilepton channel. The ratio between the data and prediction is also shown. The grey
     area shows the statistical and systematic uncertainty on the signal and background. 
     The $t\bar{t}V$, diboson and fake-lepton backgrounds are shown together in the "Others" category.
     Only the events passing the kinematic reconstruction are considered in the distributions.} 
   \label{fig:sel_dataMC}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[scale=0.33]{figures/reco/all_top_pt_all_nominal.pdf}
   \includegraphics[scale=0.33]{figures/reco/all_top_eta_all_nominal.pdf}
   \includegraphics[scale=0.33]{figures/reco/all_ttbar_pt_all_nominal.pdf}
   \includegraphics[scale=0.33]{figures/reco/all_ttbar_mass_all_nominal.pdf}
   \caption{Comparison between data and
     predictions after the kinematic reconstruction in the combined dilepton
     channel. The distributions of the top quark \pt and \eta are shown, as well as the \ttbar \pt and mass.
     The ratio between the data and prediction is also shown. The grey
     area shows the statistical and systematic uncertainty on the signal and background. 
     The $t\bar{t}V$, diboson and fake-lepton backgrounds are shown together in the "Others" category.
     The last bin of the distribution corresponds to the overflow.} 
   \label{fig:reco_dataMC}
 \end{center}
\end{figure}

The final yields for each channel as well as for the inclusive channel
combining \eeme, \emume and \mumume, along with their combined statistical and systematic uncertainties, can be found in
Table~\ref{tab:event_yields}. The predictions agree with data within 
uncertainties in all channels. 


\begin{table}
  \begin{center}
    \renewcommand{\arraystretch}{1.1}
    \input{tables/yields.tex}
    \caption{Event yields of \ttbar signal, background processes and
      data after the full event selection and the kinematic reconstruction. 
      The given uncertainties
      correspond to the combination of statistical and systematic uncertainties of the
      individual processes.  The last column represents the inclusive
      dilepton channel.}
    \label{tab:event_yields}
  \end{center}
\end{table}

