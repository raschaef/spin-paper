\section{Event selection and background estimation}
\label{sec:selection}

Reconstructed objects such as electrons, muons or jets
are built 
from the detector information and used to form a \ttbar-enriched
sample by applying an event selection.

\subsection{Object selection}
Electron candidates are reconstructed by matching inner-detector tracks to clusters in
the electromagnetic calorimeter. A requirement on the pseudorapidity of the
cluster $|\eta_{\text{cl}}| < 2.47$ is applied, with the
transition region between barrel and endcap
corresponding to
$1.37 < |\eta_{\text{cl}}| < 1.52$ excluded. A minimum requirement on the transverse
momentum (\pt) of 25~\GeV\ is applied to match the trigger criteria (see Section~\ref{sec:obj_sel}). 
Furthermore, electron candidates are required to be isolated from additional activity in the
detector. Two different criteria are used. The first one considers the activity in the electromagnetic calorimeter in a cone of size $\Delta R = 0.2$ around the electron.
The second one sums the $p_T$ of all tracks in a cone of size 0.3 around the electron track. 
The requirements applied on both variables are $\eta$-dependent and correspond to an efficiency on signal electrons of 90$\%$. 
The final selection efficiency for the electrons used in this
analysis is between 85\% and 90\% depending on the \pt and $\eta$ of the
electron~\cite{elec_eff}. 
%The probability for a fake electron to be accepted with this selection is around 0.5\%.

Muon candidates are reconstructed by combining inner detector tracks with
tracks constructed in the muon spectrometer. They are required to
have a \pt $> 25$ \GeV\ and $|\eta| < 2.5$. 
They are also required to be isolated from additional activity in the inner detector.
An isolation criterion
requiring the scalar sum of track \pt around the muon in a cone of size 
$\Delta R = 10 \text{ \GeV}/p_{\text{T}}^{\mu}$ to be less than
$0.05p_{\text{T}}^{\mu}$ is applied. 
Muons have a selection 
efficiency of about 95\%~\cite{Aad:2014rra}.

Jets are reconstructed from energy clusters in the electromagnetic and
hadronic calorimeters. The reconstruction algorithm used is the
anti-$k_{t}$~\cite{antikt_ref} algorithm with a radius parameter of
$R=0.4$. 
The measured energy of the jets is corrected to the hadronic scale using \pt- and $\eta$-dependent scale factors derived from simulation and validated in
data~\cite{atlas_jes}.
After the energy correction, they are required to have a transverse momentum $p_{\text{T}}
> 25$ \GeV\ and a pseudorapidity $|\eta| < 2.5$. For jets with
$p_{\text{T}} < 50$ \GeV\ and $|\eta| < 2.4$, the jet vertex fraction
(JVF) must be greater than 0.5. The JVF is defined as the fraction
of the scalar \pt sum of tracks associated with the jet and the primary vertex 
and the scalar \pt sum of tracks associated with the jet and any vertex. It 
distinguishes between jets originating from the primary vertex and jets
with a large contribution from other proton interactions in the same bunch crossing (pile-up).
If separated by $\Delta R < 0.2$, the jet closest to a
selected electron is removed to avoid double-counting of electrons reconstructed as jets. 
Next, all electrons and muons separated from a jet by $\Delta R < 0.4$ are removed 
from the list of selected leptons to reject semileptonic decays within a jet. Jets containing $b$-hadrons are identified
($b$-tagged) by using a multivariate algorithm
(MV1)~\cite{Aad:2015ydr} which uses
information about the tracks and secondary vertices. If the MV1 output for a
jet is larger than a predefined value, the jet is considered to be 
$b$-tagged. The value was chosen to achieve a $b$-tagging
efficiency of 70\%. With this algorithm, the probability to select 
a light jet (from gluons or $u$-, $d$-, $s$-quarks) is around 0.8\%, and the probability
to select a jet from a $c$-quark is 20\%.%, evaluated from a \ttbar MC sample.

The missing transverse momentum \met is defined
as the magnitude of the negative vectorial sum of the transverse momenta of leptons, photons 
and jets, as well as energy deposits in the calorimeter not  
associated with any physics object~\cite{atlas_met}.



\subsection{Event selection}
\label{sec:obj_sel}
The event selection aims at maximising the fraction of \ttbar
events with a dileptonic final state. 
The final states are then separated according to the 
lepton flavours. Tau leptons  
are indirectly considered in the signal contribution when decaying leptonically. This leads to three
different channels (\eeme, \mumume, \emume). Different
kinematic requirements have to be applied for the \emume and \eeme/\mumume
channels due to their different background contributions. Only events selected
from dedicated electron or muon triggers are considered. The \pt
thresholds of the triggers are 24 \GeV\ for isolated leptons and 60 (36)
\GeV\ for single-electron (-muon) triggers without an isolation requirement. 
Events containing muons compatible with cosmic-ray interactions are removed.
Exactly two oppositely
charged electrons or muons with  
$p_{\text{T}} > 25$ \GeV\ are required.
A requirement on the dilepton invariant mass
of $m_{\ell\ell} > 15$ \GeV\ is required in all channels.
In addition, $|m_{\ell\ell} - m_{Z}| > 10$ \GeV,
where $m_{Z}$ is the $Z$ boson mass, is
required in the \eeme and \mumume channels to suppress the Drell--Yan
background. In these channels the missing transverse
momentum is required to be greater than 30~\GeV. In the \emume channel, the scalar
sum of the $p_{\text{T}}$ of the jets and leptons in the event
($H_{\text{T}}$) is required to be $H_{\text{T}} > 130$
\GeV. At least two jets with at least one of them being $b$-tagged are required in each channel.

\subsection{Background estimation}
\label{sec:bkg}

Single-top-quark and diboson backgrounds are estimated using MC simulation only.
The MC estimate for the Drell--Yan and fake-lepton background is normalised 
using data-driven scale factors (SF).
The Drell--Yan background does not contain any real \met. 
Non-negligible \met can appear in a fraction of events with
misreconstructed objects, which are difficult to model. 
Since real \met is present in $Z \rightarrow \tau\tau$ events,
no scale factors are applied to this sample.
Another issue is the correct normalisation of
Drell--Yan events with additional heavy-flavour (HF) jets from $b$- and $c$-quarks after the
$b$-tagging requirement.
In order to correct for these effects, three 
control regions are defined, from which three SF are extracted. 
Two correspond to the \met modelling in $Z \rightarrow ee$ (SF$_{ee}$) and $Z \rightarrow \mu\mu$ events
(SF$_{\mu\mu}$), and one for the heavy-flavour normalisation in
$Z$+jets events (SF$_{\text{HF}}$)
common to the three dilepton channels. 
All control regions require the same
selection as the signal region with the exception that the invariant mass of the 
two leptons should be within 10~\GeV\ of the $Z$ mass.
The control regions are then distinguished by dividing them
into a pretag ($n_{b\mhyphen\text{tag}} \geq 0$) and a $b$-tag region
($n_{b\mhyphen\text{tag}} \geq 1$), additionally dividing the pretag
region into the \eeme and \mumume channels.
The purity of the pretag control region is 97$\%$ on average for both channels. The purity of the $b$-tag region is 75$\%$.
The SF are extracted by solving a system of equations which relates 
the number of events in data and in simulation in the three control regions.
The lepton-flavour-dependent scale factors SF$_{ee/\mu\mu}$ are $0.927 \pm 0.005$
and $0.890 \pm 0.004$ respectively for the $ee$ and $\mu\mu$ channels 
while the heavy-flavour scale factor SF$_{\text{HF}}$ is $1.70 \pm 0.03$, where the uncertainties
are only statistical.

The shape of the fake and non-prompt lepton background distributions are taken from 
MC simulation but the normalisation is derived from data in a control 
region enriched in fake leptons.
This is achieved by applying the same requirements as for the
signal region, except that two leptons of the same charge are required. As
fake leptons have approximately the same probability of having
negative or positive charge, the same number of fake-lepton events should 
populate the opposite-sign and same-sign selection regions. The
same-sign control region has a smaller background contribution from
other processes, allowing the study of the modelling of the fake-lepton background. Channel-dependent scale factors are derived
by normalising the predictions to data in the control regions, while the
shapes of the distributions are taken from MC simulation. The SF in the \eeme and \emume channels are around 1.0 and 1.5, whereas the SF in the \mumume
channel is
about 4. The differences between the three scale factors originate from the 
sources of misidentified electrons and muons, which seem to be
modelled better in MC simulation for the electrons. However, the shapes of the
distributions of several kinematic variables in the \mumume channel are cross-checked in control regions and found to be consistent with the distributions from a purely data-driven method. The relative statistical uncertainties are about 20\% in the
same-flavour channels and 10\% in the \emume channel.

\input{PolSpinPaper/Reconstruction}
