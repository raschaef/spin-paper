\subsection{Unfolding}
\label{sec:unfolding}

Selection requirements and detector resolution distort the
reconstructed distributions. An unfolding procedure is applied to correct for these
distortions. The Fully Bayesian Unfolding~\cite{FBU} method is used.
It is based on Bayes' theorem and estimates 
the probability ($p$) of $\Truth{}\in{}\Real{}^{N_t}$ being the true spectrum given the observed
data $\Data\in\Integer^{N_r}$.\footnote{$\Real{}$ and $\Integer$ are the sets of real and natural numbers. $N_{t}$
and $N_{r}$ are the number of bins for the true and reconstructed
distributions.}
This probability is proportional to the likelihood ($\mathcal{L}$) of obtaining the data distribution given
a true spectrum and a response matrix
$\TrasfMatrix\in\Real{}^{N_r}\times{}\Real{}^{N_t}$.
This can be expressed as
\begin{equation}
  \conditionalProb{\Truth{}}{\Data{},\TrasfMatrix{}}
  \propto{}
  \conditionalLhood{\Data{}}{\Truth{},\TrasfMatrix{}}
  \cdot{}
  \pi{}\left(\Truth{}\right),
\end{equation}

where $\pi{}$ is the prior probability density for the true spectrum $\Truth{}$ 
and is taken to be uniform. The
background is estimated as described in Section~\ref{sec:bkg} 
and included in the computation of the likelihood by taking into account
its contribution in data when comparing it with the true spectrum.
The response matrix $\TrasfMatrix{}$, in which each entry $\TrasfMatrix{}_{ij}$ gives the
probability of an event generated in bin $i$ to be reconstructed in
bin $j$, is calculated from the nominal
signal sample. 
By taking a rectangular response matrix connecting the
three different analysis channels to the same true spectrum, the
channels are combined within the unfolding method. The unfolded value
is taken to be the mean of the posterior distribution with its root mean square
taken as the uncertainty.

Different systematic uncertainties are estimated within the unfolding
by adding nuisance parameter terms ($\boldsymbol{\theta}$) to the likelihood for each systematic
uncertainty considered. The so-called marginal likelihood is then
defined as
\begin{equation}
\conditionalLhood{\Data{}}{\Truth{}} = \int \conditionalLhood{\Data{}}{\Truth{},\boldsymbol{\theta}}\cdot \pi(\boldsymbol{\theta})\textrm{d}\boldsymbol{\theta},
\end{equation}

where $\pi(\boldsymbol{\theta})$ is the prior probability density 
for each nuisance parameter $\boldsymbol{\theta}$.
They are defined as Gaussian distributions $G$
with a mean of zero and a width of one. Systematic uncertainties can
be distinguished between normalisation-changing uncertainties ($\theta_n$) and uncertainties
changing both the normalisation and the shape ($\theta_s$) of the reconstructed
distribution of signal $R(\Truth{}; \theta_s)$ and background
$B(\theta_s, \theta_n)$. The marginal likelihood can
then be expressed as:
\begin{equation}
\conditionalLhood{\Data{}}{\Truth{}} = \int \conditionalLhood{\Data{}}{R(\Truth{}; \theta_s),B(\theta_s, \theta_b)}\cdot G(\theta_s) \cdot G(\theta_b) \textrm{d}\theta_s \textrm{d}\theta_b.
\end{equation}

The method is validated by performing a linearity test in which distributions with 
known values of the polarisation and spin correlations are unfolded.
The distributions of observables are reweighted to inject different values of
the polarisations and correlations. 
For the polarisations and
spin correlations, the double-differential cross section (Equation~(\ref{eqn:double-diff})) is
used, while a linear reweighting is used for the cross
correlations.
The unfolded value for each reweighted distribution 
is then compared to the true value of polarisation or spin correlation and a calibration curve is built.
Non-closure in the linearity test appears as a slope different from one in the
calibration curve.
The number of bins and the bin widths for each observable are chosen based on its resolution
and optimised by evaluating the expected statistical uncertainty and by limiting the bias 
in the linearity test.
The binning optimisation leads to a four-bin configuration
for the polarisation observables and six-bin configurations for the
different correlation observables.
An uncertainty is added to cover the non-closure of the linearity test, which is at most
10\%.
The input distribution and the response matrix normalised per true bin
are shown for one example of polarisation, spin
correlation, and cross correlation in Figures~\ref{fig:unf_input_dataMC} and ~\ref{fig:unf_resmat}. 

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.7\linewidth]{figures/reco/partonic_helpol_plus_channelsep_ratio_split_final.pdf}
   \includegraphics[width=0.7\linewidth]{figures/reco/partonic_transcorr_channelsep_ratio_split_final.pdf}
   \includegraphics[width=0.7\linewidth]{figures/reco/partonic_transrdiff_channelsep_ratio_split_final.pdf}
   \caption{Input distributions for the unfolding procedure of
     \helcosplus, \transcoscos, and \transrcoscosdiff.
     The ratio between the data and prediction is also shown. The grey
     area shows the total uncertainty on the signal and background. 
     The $t\bar{t}V$, diboson and fake-lepton backgrounds are shown together in the "Others" category.} 
   \label{fig:unf_input_dataMC}
 \end{center}
\end{figure}

\begin{figure}[!htbp]
 \begin{center}
   \includegraphics[width=0.8\linewidth]{figures/resmats/resmat_helpol_plus_full.pdf}
   \\[1cm]
   \includegraphics[width=0.8\linewidth]{figures/resmats/resmat_transcorr_full.pdf}
   \\[1cm]
   \includegraphics[width=0.8\linewidth]{figures/resmats/resmat_transrdiff_full.pdf}
   \caption{Response matrices of observables 
     \helcosplus, \transcoscos, and \transrcoscosdiff.
     at parton level. They are divided into $ee$, $\mu\mu$, and $e\mu$ channels. The matrices are normalised per
     truth bin (rows) for each channel separately.} 
   \label{fig:unf_resmat}
 \end{center}
\end{figure}
